use icmp;
use ip;
use common;

extern crate libc;
use libc::{AF_INET, SOCK_RAW, IPPROTO_RAW, SO_RCVTIMEO, SOL_SOCKET, EAGAIN, EINTR};
use libc::{c_int, c_void, sockaddr_in, in_addr, timeval};
use libc::{socket, sendto, recvfrom, setsockopt};

use std::mem;
use std::io;

const ICMP_FILTER: c_int = 1;

pub fn create_icmp_socket(timeout: i64, icmp_filter: u32) -> Result<c_int, String> {
    let socket_handle = match unsafe {
        socket(AF_INET, SOCK_RAW, icmp::PROTO_ICMP as i32) }
    {
        err if err < 0 => {
            return Err(format!("socket failed: {}", io::Error::last_os_error()));
        },
        socket_handle => socket_handle
    };

    let rc = unsafe {
        setsockopt(socket_handle, libc::IPPROTO_RAW, ICMP_FILTER,
                   &icmp_filter as *const _ as *const c_void,
                   mem::size_of::<u32>() as libc::socklen_t)
    };

    if rc < 0 {
        return Err(format!("setsockopt failed: {}", io::Error::last_os_error()));
    }

    let timeout = libc::timeval {
        tv_sec: timeout,
        tv_usec: 0
    };

    let rc = unsafe {
        setsockopt(socket_handle, libc::SOL_SOCKET, libc::SO_RCVTIMEO,
                   &timeout as *const _ as *const c_void,
                   mem::size_of::<libc::timeval>() as libc::socklen_t)
    };
    if rc < 0 {
        return Err(format!("setsockopt failed: {}", io::Error::last_os_error()));
    }

    Ok(socket_handle)
}

pub fn send_packet(socket_handle: c_int, dest_ip: u32, packet: *const c_void, len: u64)
    -> Result<bool, String>
{
    let sockaddr = libc::sockaddr_in {
        sin_family: AF_INET as u16,
        sin_port: 0,
        sin_addr: in_addr {
            s_addr: dest_ip,
        },
        sin_zero: [0u8; 8]
    };

    let addrlen = mem::size_of::<libc::sockaddr_in>() as libc::socklen_t;

    match unsafe {
        sendto(socket_handle,
               packet,
               len,
               0,
               &sockaddr as *const _ as *const libc::sockaddr,
               addrlen) }
    {
        err if err < 0 => {
            let error = io::Error::last_os_error();
            match error.raw_os_error().unwrap() {
                libc::EINTR => {
                    return Ok(true);
                },
                _ => {
                    return Err(format!("sendto failed: {}", error));
                }
            }
        },
        bytes_sent if bytes_sent as u64 != len => {
            return Err("Not all bytes were sent!".to_string());
        },
        _ => {
            // Success
        },
    };

    Ok(false)
}

pub enum RecvIcmpResult {
    Received(u64, ip::IpPacket, icmp::IcmpReply),
    Timeout,
    Interrupted,
}

// TODO: return value is quite ugly
pub fn recv_icmp_packet(socket_handle: c_int, dest_ip: u32)
    -> Result<RecvIcmpResult, String> {
    let mut sockaddr = libc::sockaddr_in {
        sin_family: AF_INET as u16,
        sin_port: 0,
        sin_addr: in_addr {
            s_addr: dest_ip
        },
        sin_zero: [0u8; 8]
    };

    let mut addrlen = mem::size_of::<libc::sockaddr_in>() as libc::socklen_t;

    // raw(7): "Packet can be sent without IP header but they are always received
    // with IP header.", so read the IP header in addition to ICMP.
    let mut replybuf = [0u8; ip::IP_PACKET_LENGTH + icmp::ICMP_DEST_UNREACHABLE_LENGTH];

    let bytes_recvd = match unsafe {
        recvfrom(socket_handle,
                 &mut replybuf as *mut _ as *mut c_void,
                 replybuf.len() as u64, 0,
                 &mut sockaddr as *mut _ as *mut libc::sockaddr,
                 &mut addrlen) }
    {
        err if err < 0 => {
            let error = io::Error::last_os_error();
            match error.raw_os_error().unwrap() {
                // Timeout. Should also check EWOULDBLOCK, but that equals
                // to EAGAIN and that doesn't compile..
                libc::EAGAIN => {
                    return Ok(RecvIcmpResult::Timeout);
                },
                libc::EINTR => {
                    return Ok(RecvIcmpResult::Interrupted);
                },
                _ => {
                    return Err(format!("recvfrom failed: {}", error));
                }
            }
        },
        bytes_recvd if (bytes_recvd as usize) < ip::IP_PACKET_LENGTH => {
            return Err(format!("Too small packet received {}", bytes_recvd));
        },
        bytes_recvd => bytes_recvd
    };

    let ip_packet = ip::IpPacket::new(&replybuf[0..ip::IP_PACKET_LENGTH]);

    if !ip_packet.is_icmp_packet() {
        common::print_packet(&replybuf);
        return Err("".to_string());
    }

    let icmp_packet = icmp::IcmpReply::new(&replybuf[ip::IP_PACKET_LENGTH..]);

    Ok(RecvIcmpResult::Received(bytes_recvd as u64, ip_packet, icmp_packet))
}
