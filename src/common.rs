use time::Duration;
use std::u16;

pub fn u8_to_u16(a: u8, b: u8) -> u16 {
    ((a as u16) << 8u16) + b as u16
}

pub fn u8_to_u32(a: u8, b: u8, c: u8, d: u8) -> u32 {
    ((a as u32) << 24u32) + ((b as u32) << 16u32) + ((c as u32) << 8u32) + d as u32
}

pub fn truncate_u32_to_u16(a: u32) -> (bool, u16) {
    if a >= u16::MAX as u32 {
        (true, u16::MAX)
    } else {
        (false, a as u16)
    }
}

pub fn ones_complement_add(a: u16, b: u16) -> u16 {
    let c = a as u32 + b as u32;

    c as u16 + (c >> 16u32) as u16
}

pub fn duration_to_millis(duration: &Duration) -> f32 {
    duration.num_milliseconds() as f32
        + duration.num_microseconds().unwrap_or(0) as f32 / 1000.0
}

pub fn print_packet(packet: &[u8]) {
    for i in packet {
        print!("{:02x} ", i);
    }
    println!("");
}

#[test]
fn test_ones_complement() {
    assert_eq!(0x9a01, ones_complement_add(0xef00, 0xab00));
    assert_eq!(0xffff, ones_complement_add(0xffff, 0xffff));
}
