use common;
use icmp;

// Note: Not packed, larger than real IP packet
#[derive(Debug, PartialEq, Eq)]
pub struct IpPacket {
    // TODO: no bitfields?
    pub version: u8,
    pub ihl: u8,
    pub dscp: u8,
    pub ecn: u8,
    pub total_len: u16,
    pub identification: u16,
    pub flags: u8,
    pub offset: u16,
    pub ttl: u8,
    pub protocol: u8,
    pub checksum: u16,
    pub src: u32,
    pub dst: u32,
}

pub const IP_PACKET_LENGTH: usize = 20;

impl IpPacket {
    pub fn new(buf: &[u8]) -> IpPacket {
        if buf.len() != IP_PACKET_LENGTH {
            panic!("Invalid IpPacket buf, len: {}", buf.len());
        }

        IpPacket { version: buf[0] >> 4,
                  ihl: buf[0] & 0xf,
                  dscp: buf[1] >> 2,
                  ecn: buf[1] & 0x3,
                  total_len: common::u8_to_u16(buf[2], buf[3]),
                  identification: common::u8_to_u16(buf[4], buf[5]),
                  flags: buf[6] >> 5,
                  offset: common::u8_to_u16(buf[6] & 0x3f, buf[7]),
                  ttl: buf[8],
                  protocol: buf[9],
                  checksum: common::u8_to_u16(buf[10], buf[11]),
                  src: common::u8_to_u32(buf[12], buf[13], buf[14], buf[15]),
                  dst: common::u8_to_u32(buf[16], buf[17], buf[18], buf[19]),
                 }
    }

    pub fn is_icmp_packet(&self) -> bool {
        if self.version != 4 {
            println!("Invalid IP version: {}", self.version);
            return false;
        }

        if self.ihl != 5 {
            println!("Invalid IHL: {}", self.ihl);
            return false;
        }

        if self.protocol != icmp::PROTO_ICMP {
            println!("Invalid protocol: {}", self.protocol);
            return false;
        }

        if (self.total_len as usize) < (IP_PACKET_LENGTH + icmp::ICMP_ECHO_LENGTH) {
            println!("Invalid length: {}", self.total_len);
            return false;
        }

        // TODO: check dest address

        true
    }
}

#[test]
fn test_ip_new() {
    let received_ip_packet: [u8; 20] = [0x45, 0x0, 0x0, 0x20, 0x78, 0xbd, 0x0, 0x0, 0x40, 0x1,
                                        0x4, 0x1e, 0x7f, 0x0, 0x0, 0x1, 0x7f, 0x0, 0x0, 0x1];
    let expected_packet = IpPacket { version: 4, ihl: 5, dscp: 0, ecn: 0, total_len: 32,
                                    identification: 30909, flags: 0, offset: 0, ttl: 64,
                                    protocol: 1, checksum: 1054, src: 2130706433, dst: 2130706433 };

    assert_eq!(expected_packet, IpPacket::new(&received_ip_packet));
}

#[test]
fn test_ip_new_longer_packet() {
    let received_ip_packet: [u8; 20] = [0x45, 0xc0, 0x00, 0x3c, 0x6a, 0x8a, 0x00,
                                        0x00, 0x40, 0x01, 0x40, 0xe7, 0x52,
                                        0x82, 0x14, 0xc6, 0x52, 0x82, 0x14, 0xc6];

    let expected_packet = IpPacket { version: 4, ihl: 5, dscp: 0x30, ecn: 0, total_len: 60,
                                    identification: 27274, flags: 0, offset: 0, ttl: 64,
                                    protocol: 1, checksum: 0x40e7,
                                    src: 1384256710, dst: 1384256710 };

    assert_eq!(expected_packet, IpPacket::new(&received_ip_packet));
}

#[test]
#[should_panic]
fn test_ip_new_too_short_packet() {
    let received_ip_packet: [u8; 19] = [0x45, 0x0, 0x0, 0x20, 0x78, 0xbd, 0x0, 0x0, 0x40, 0x1,
                                        0x4, 0x1e, 0x7f, 0x0, 0x0, 0x1, 0x7f, 0x0, 0x0];
    IpPacket::new(&received_ip_packet);
}
