use std::env;
use std::net::Ipv4Addr;
use std::str::FromStr;

extern crate ping;

use std::u16;

const ENDLESS_COUNT: u32 = u16::MAX as u32 + 1;

#[cfg_attr(test, allow(dead_code))]
fn main() {
    fn get_usage() -> String {
        format!("{}: <ip>",
                env::args().nth(0).unwrap())
    }

    if env::args_os().count() != 2 {
        println!("Usage: {}", get_usage());
        std::process::exit(-1);
    }

    let ip = match Ipv4Addr::from_str(&env::args().nth(1).unwrap()) {
        Err(err) => {
            println!("{}\nUsage: {}", err, get_usage());
            std::process::exit(-1);
        },
        Ok(ip) => ip,
    };

    if !ping::can_use_raw_sockets() {
        println!("Cannot use raw sockets");
        std::process::exit(-1);
    }

    match ping::ping_ip(ip, ENDLESS_COUNT) {
        Err(err) => { println!("{}", err); },
        Ok(_) => {},
    }
}
