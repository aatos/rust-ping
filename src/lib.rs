mod common;
mod ip;
mod icmp;
mod socket;

extern crate libc;
use libc::{c_int, c_void};
use libc::{geteuid};

use std::net::Ipv4Addr;
use std::fmt;

extern crate time;
use time::{PreciseTime, Duration};

extern crate ctrlc;
use ctrlc::CtrlC;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use std::time::Duration as stdDuration;

const RECV_TIMEOUT_SECONDS: i64 = 5;
const TIME_BETWEEN_ECHOS_MILLIS: u64 = 500;

enum PingResult {
    Timeout {
        duration: Duration
    },
    Result {
        bytes_received: u64,
        ip: Ipv4Addr,
        seq_num: u16,
        ttl: u8,
        rtt: Duration,
    },
    Unreachable {
        ip: Ipv4Addr,
        rtt: Duration,
        seq_num: u16,
        unreachable_type: String,
    },
    None,
}

impl PingResult {
    fn new_result(bytes_received: u64, ip: Ipv4Addr, seq_num: u16, ttl: u8, rtt: Duration)
        -> PingResult {
        PingResult::Result {
            bytes_received: bytes_received,
            ip: ip,
            seq_num: seq_num,
            ttl: ttl,
            rtt: rtt
        }
    }
    fn new_timeout(duration: Duration) -> PingResult {
        PingResult::Timeout {
            duration: duration
        }
    }
    fn new_unreachable(ip: Ipv4Addr, rtt: Duration, seq_num: u16, unreachable_type: &str)
        -> PingResult {
        PingResult::Unreachable {
            ip: ip,
            rtt: rtt,
            seq_num: seq_num,
            unreachable_type: unreachable_type.to_string(),
        }
    }
}

impl fmt::Display for PingResult {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PingResult::Result { bytes_received, ip, seq_num, ttl, rtt } => {
                write!(f, "{} bytes from {}: icmp_seq={}, ttl={}, rtt={:.3} ms",
                       bytes_received, ip, seq_num, ttl, common::duration_to_millis(&rtt))
            },
            PingResult::Timeout { duration } => {
                write!(f, "No reply received in {:.3} ms", common::duration_to_millis(&duration))
            },
            PingResult::Unreachable { ip, seq_num, ref unreachable_type, .. } => {
                write!(f, "From {} icmp_seq={} Destination {} Unreachable", ip, seq_num,
                       unreachable_type)
            },
            PingResult::None => {
                panic!("Cannot print None");
            },
        }
    }
}

struct Ping {
    ip: Ipv4Addr,
    socket_handle: c_int,
    icmp_request: icmp::PackedIcmpEcho,
    // TODO: Use payload for this
    request_sent: PreciseTime,
    sleep_time: stdDuration,
}

impl Ping {
    fn new(ip: Ipv4Addr, identifier: u16, payload: u32) -> Ping {
        let socket_handle = match socket::create_icmp_socket(RECV_TIMEOUT_SECONDS,
                                                             1 << icmp::ICMP_ECHO_REQUEST) {
            Err(err) => panic!(err),
            Ok(socket_handle) => socket_handle,
        };

        let request = icmp::PackedIcmpEcho {
            icmp_type: icmp::ICMP_ECHO_REQUEST,
            code: 0u8,
            checksum: 0u16,
            identifier: identifier.to_be(),
            seq_num: 0u16,
            payload: payload.to_be()
        };

        Ping {
            ip: ip, socket_handle: socket_handle, icmp_request: request,
            request_sent: PreciseTime::now(),
            sleep_time: stdDuration::from_millis(TIME_BETWEEN_ECHOS_MILLIS)
        }
    }

    fn send_request(&mut self, seq_num: u16) -> Result<bool, String> {
        self.icmp_request.seq_num = seq_num.to_be();
        self.icmp_request.set_checksum();

        self.request_sent = PreciseTime::now();

        socket::send_packet(self.socket_handle, u32::from(self.ip).to_be(),
                            &self.icmp_request as *const _ as *const c_void,
                            icmp::ICMP_ECHO_LENGTH as u64)
    }

    fn wait_for_reply(&mut self) -> Result<PingResult, String> {

        let (bytes_recvd, ip_packet, icmp_packet) =
            match socket::recv_icmp_packet(self.socket_handle,
                                           u32::from(self.ip).to_be())
        {
            Err(error) => {
                return Err(error)
            },
            Ok(socket::RecvIcmpResult::Timeout) => {
                return Ok(PingResult::new_timeout(self.request_sent.to(PreciseTime::now())));
            },
            Ok(socket::RecvIcmpResult::Interrupted) => {
                return Ok(PingResult::None);
            },
            Ok(socket::RecvIcmpResult::Received(bytes_recvd, ip_packet, icmp_packet)) => {
                (bytes_recvd, ip_packet, icmp_packet)
            },
        };

        let reply_received = PreciseTime::now();

        if !icmp_packet.is_reply_for(&self.icmp_request) {
            panic!("Unimplemented: reply was not for our ICMP echo request");
        }

        match icmp_packet {
            icmp::IcmpReply::IcmpEcho { seq_num, .. } => {
                Ok(PingResult::new_result(bytes_recvd - ip::IP_PACKET_LENGTH as u64,
                                          Ipv4Addr::from(ip_packet.src),
                                          seq_num, ip_packet.ttl,
                                          self.request_sent.to(reply_received)))
            },
            icmp::IcmpReply::IcmpDestUnreachable { seq_num, code, .. } => {
                Ok(PingResult::new_unreachable(Ipv4Addr::from(ip_packet.src),
                                               self.request_sent.to(reply_received),
                                               seq_num,
                                               icmp::IcmpReply::unreachable_code_to_string(code)))
            },
        }
    }
}

fn install_ctrlc_handler(atomic_bool: &Arc<AtomicBool>) {
    let c = atomic_bool.clone();

    CtrlC::set_handler(move || {
        c.store(false, Ordering::SeqCst);
    });
}

pub fn ping_ip(ip: Ipv4Addr, count: u32) -> Result<usize, String> {
    let mut ping = Ping::new(ip, 0xafaf, 0x12abcdef);

    println!("PING {} {}({}) bytes of data.", ip,
             icmp::ICMP_ECHO_PAYLOAD_LENGTH,
             ip::IP_PACKET_LENGTH + icmp::ICMP_ECHO_LENGTH);

    let is_running = Arc::new(AtomicBool::new(true));
    install_ctrlc_handler(&is_running);

    let mut duration = Duration::zero();
    let mut replies_received = 0;
    let mut replies_sent = 0;
    let mut errors = 0;

    'outer: loop {
        let (is_endless, count) = common::truncate_u32_to_u16(count);

        for i in 0..count {
            let interrupted = try!(ping.send_request(i));
            if interrupted {
                break 'outer;
            }

            replies_sent += 1;

            let ping_result = try!(ping.wait_for_reply());

            let rtt = match ping_result {
                PingResult::None => { break 'outer },
                PingResult::Result { rtt, .. } => { replies_received += 1; rtt },
                PingResult::Timeout { duration } => duration,
                PingResult::Unreachable { rtt, .. } => { errors += 1; rtt },
            };

            duration = duration + rtt;

            println!("{}", ping_result);

            if !is_running.load(Ordering::SeqCst) {
                break 'outer;
            }

            std::thread::sleep(ping.sleep_time);
        }

        if !is_endless {
            break;
        }
    }

    println!("\n--- {} ping statistics ---", ip);
    println!("{} packets transmitted, {} received, {} errors, {:.0}% packet loss, time {:.3} ms",
             replies_sent, replies_received,
             errors, 100.0 - (replies_received as f32 / replies_sent as f32) * 100.0,
             common::duration_to_millis(&duration));

    Ok(1)
}

pub fn can_use_raw_sockets() -> bool {
    let euid = unsafe {
        geteuid()
    };
    euid == 0
}
