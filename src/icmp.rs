use common;
use ip;

pub const PROTO_ICMP: u8 = 1;
pub const ICMP_ECHO_REPLY: u8 = 0;
pub const ICMP_ECHO_REQUEST: u8 = 8;
pub const ICMP_DEST_UNREACHABLE: u8 = 3;

pub const ICMP_ECHO_PAYLOAD_LENGTH: usize = 4;
pub const ICMP_ECHO_LENGTH: usize = 8 + ICMP_ECHO_PAYLOAD_LENGTH;
pub const ICMP_DEST_UNREACHABLE_LENGTH: usize = 8 + ip::IP_PACKET_LENGTH + ICMP_ECHO_LENGTH;

#[derive(Debug, PartialEq, Eq)]
pub enum IcmpReply {
    IcmpEcho {
        icmp_type: u8,
        code: u8,
        checksum: u16,
        identifier: u16,
        seq_num: u16,
        payload: u32,
    },
    IcmpDestUnreachable {
        icmp_type: u8,
        code: u8,
        checksum: u16,
        unused: u16,
        next_hop_mtu: u16,
        ip_header: ip::IpPacket,
        seq_num: u16,
    }
}

impl IcmpReply {
    pub fn new(buf: &[u8]) -> IcmpReply {
        match buf[0] {
            ICMP_ECHO_REPLY | ICMP_ECHO_REQUEST => {
                if buf.len() < ICMP_ECHO_LENGTH {
                    panic!("Invalid IcmpEcho buf, len: {}", buf.len());
                }

                IcmpReply::IcmpEcho { icmp_type: buf[0],
                                         code: buf[1],
                                         checksum: common::u8_to_u16(buf[2], buf[3]),
                                         identifier: common::u8_to_u16(buf[4], buf[5]),
                                         seq_num: common::u8_to_u16(buf[6], buf[7]),
                                         payload: common::u8_to_u32(buf[8], buf[9], buf[10], buf[11]),
                                        }
            },
            ICMP_DEST_UNREACHABLE => {
                if buf.len() < ICMP_DEST_UNREACHABLE_LENGTH {
                    panic!("Invalid IcmpDestUnreachable buf, len: {}", buf.len());
                }

                let seq_num = match IcmpReply::new(&buf[(8 + ip::IP_PACKET_LENGTH)..]) {
                    IcmpReply::IcmpEcho { seq_num, .. } => seq_num,
                    _ => panic!("Invalid IcmpEcho inside IpPacket"),
                };

                IcmpReply::IcmpDestUnreachable {
                    icmp_type: buf[0],
                    code: buf[1], // TODO: should print this in PingResult
                    checksum: common::u8_to_u16(buf[2], buf[3]),
                    unused: common::u8_to_u16(buf[4], buf[5]),
                    next_hop_mtu: common::u8_to_u16(buf[6], buf[7]),
                    ip_header: ip::IpPacket::new(&buf[8..(8 + ip::IP_PACKET_LENGTH)]),
                    seq_num: seq_num,
                }
            },
            code => {
                panic!("Unhandled ICMP code: {}", code)
            },

        }
    }

    pub fn unreachable_code_to_string(code: u8) -> &'static str {
        match code {
            1 => "Host",
            _ => panic!("Unimplemented destination unreachable code"),
        }
    }

    pub fn is_reply_for(&self, icmp_packet: &PackedIcmpEcho) -> bool {
        match *self {
            IcmpReply::IcmpEcho { identifier, seq_num, payload, code, .. } => {

                if icmp_packet.code != code {
                    println!("Invalid code: {}", icmp_packet.code);
                    return false;
                }

                if icmp_packet.identifier.to_be() != identifier {
                    println!("Invalid identifier: {}", icmp_packet.identifier);
                    return false;
                }

                if icmp_packet.seq_num.to_be() != seq_num  {
                    println!("Invalid seq_num: {}", icmp_packet.seq_num);
                    return false;
                }

                if icmp_packet.payload.to_be() != payload  {
                    println!("Invalid payload: {}", icmp_packet.payload);
                    return false;
                }

                // TODO: check checksum

                true
            },
            IcmpReply::IcmpDestUnreachable { seq_num, .. } => {
                // TODO: check other fields

                if icmp_packet.seq_num.to_be() != seq_num  {
                    println!("Invalid seq_num: {}", icmp_packet.seq_num);
                    return false;
                }

                true
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
#[repr(packed)]
pub struct PackedIcmpEcho {
    pub icmp_type: u8,
    pub code: u8,
    pub checksum: u16,
    pub identifier: u16,
    pub seq_num: u16,
    pub payload: u32,
}

impl PackedIcmpEcho {
    pub fn set_checksum(&mut self) {
        // TODO: This is quite ugly, define a trait instead
        self.checksum = common::ones_complement_add((self.icmp_type as u16) << 8u16,
                                                    self.code as u16);
        self.checksum = common::ones_complement_add(self.checksum,
                                                    self.identifier.to_be());
        self.checksum = common::ones_complement_add(self.checksum,
                                                    self.seq_num.to_be());
        self.checksum = common::ones_complement_add(self.checksum,
                                                    ((self.payload >> 16u32) as u16).to_be());
        self.checksum = common::ones_complement_add(self.checksum,
                                                    (self.payload as u16).to_be());
        self.checksum = !self.checksum.to_be();
    }
}

#[test]
fn test_checksum() {
    fn check_checksum(icmp_type: u8,
                      code: u8,
                      identifier: u16,
                      seq_num: u16,
                      payload: u32,
                      expected_checksum: u16) {

        let mut echo = PackedIcmpEcho {
            icmp_type: icmp_type,
            code: code,
            checksum: 0,
            identifier: identifier,
            seq_num: seq_num,
            payload: payload };

        echo.set_checksum();

        assert_eq!(expected_checksum, echo.checksum);
    }

    check_checksum(ICMP_ECHO_REQUEST,
                   0, 1u16.to_be(),
                   0u16.to_be(),
                   2u32.to_be(),
                   0xf7fcu16.to_be());

    check_checksum(ICMP_ECHO_REQUEST,
                   0, 1u16.to_be(),
                   0u16.to_be(),
                   2u32,
                   0xf5feu16.to_be());

    check_checksum(ICMP_ECHO_REQUEST,
                   0, 1u16.to_be(),
                   0u16.to_be(),
                   0xabcdu32.to_be(),
                   0x4c31u16.to_be());

    check_checksum(ICMP_ECHO_REQUEST,
                   0, 1u16.to_be(),
                   0u16.to_be(),
                   0xef01u32.to_be(),
                   0x08fdu16.to_be());

    check_checksum(ICMP_ECHO_REQUEST,
                   0, 1u16.to_be(),
                   0u16.to_be(),
                   0xcdef00u32.to_be(),
                   0x0831u16.to_be());

    check_checksum(ICMP_ECHO_REQUEST,
                   0, 1u16.to_be(),
                   0u16.to_be(),
                   0xabcdef01u32.to_be(),
                   0x5d2fu16.to_be());
}

#[test]
fn test_icmp_new() {
    let expected_icmp = IcmpReply::IcmpEcho { icmp_type: 0, code: 0, checksum: 28593,
                                             identifier: 44975, seq_num: 4, payload: 313249263 };
    let received_icmp: [u8; 12] = [0x0, 0x0, 0x6f, 0xb1, 0xaf, 0xaf, 0x0, 0x4, 0x12, 0xab,
                                   0xcd, 0xef];

    assert_eq!(expected_icmp, IcmpReply::new(&received_icmp));
}

#[test]
fn test_icmp_new_dest_unreachable() {
    let expected_ip_packet = ip::IpPacket { version: 4, ihl: 5, dscp: 0, ecn: 0, total_len: 32,
                                           identification: 10651, flags: 0x02, offset: 0, ttl: 64,
                                           protocol: 1, checksum: 0x42b6,
                                           src: 1384256710, dst: 1384256706 };

    let expected_icmp = IcmpReply::IcmpDestUnreachable {
        icmp_type: ICMP_DEST_UNREACHABLE, code: 1, checksum: 64766,
        unused: 0, next_hop_mtu: 0, ip_header: expected_ip_packet,
        seq_num: 2
    };

    let received_icmp: [u8; 40] = [0x03, 0x01, 0xfc, 0xfe, 0x00, 0x00, 0x00, 0x00,
                                   // Ip
                                   0x45, 0x00, 0x00, 0x20, 0x29, 0x9b, 0x40, 0x00,
                                   0x40, 0x01, 0x42, 0xb6, 0x52, 0x82, 0x14, 0xc6,
                                   0x52, 0x82, 0x14, 0xc2,
                                   // Icmp echo request
                                   0x08, 0x00, 0x67, 0xb5, 0xaf, 0xaf, 0x00, 0x02,
                                   0x12, 0xab, 0xcd, 0xef];

    assert_eq!(expected_icmp, IcmpReply::new(&received_icmp));
}

// TODO: test this again
// #[test]
// fn test_icmp_is_reply() {
//     let request = IcmpReply::IcmpEcho { icmp_type: 8, code: 0, checksum: 45415, identifier: 44975,
//                             seq_num: 1024, payload: 4023233298 };
//     let reply = IcmpReply::IcmpEcho { icmp_type: 0, code: 0, checksum: 28593, identifier: 44975,
//                             seq_num: 4, payload: 313249263 };

//     assert!(request.is_reply(&reply));
// }

#[test]
#[should_panic]
fn test_icmp_new_too_short_packet() {
    let received_icmp: [u8; 11] = [0x0, 0x0, 0x6f, 0xb1, 0xaf, 0xaf, 0x0, 0x4, 0x12, 0xab,
                                   0xcd];

    IcmpReply::new(&received_icmp);
}
