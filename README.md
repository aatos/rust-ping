Ping
====

Simple tool for sending ICMP echo requests and waiting for echo replies (also
known as ping).

Building
--------

```
cargo build
```

Running
-------

Note: Application uses raw sockets for sending ICMP packets (which is directly
on top of IP), which require root access. For the same reason `ping(8)` on your
machine has `setuid` attribute assigned to it.

```
Usage: ping <ip_to_ping>
```

For example:

```
$ sudo ./target/debug/ping 64.233.162.105
PING 64.233.162.105 4(32) bytes of data.
12 bytes from 64.233.162.105: icmp_seq=0, ttl=47, rtt=32.148 ms
12 bytes from 64.233.162.105: icmp_seq=1, ttl=47, rtt=32.096 ms
12 bytes from 64.233.162.105: icmp_seq=2, ttl=47, rtt=32.134 ms
12 bytes from 64.233.162.105: icmp_seq=3, ttl=47, rtt=32.107 ms
12 bytes from 64.233.162.105: icmp_seq=4, ttl=47, rtt=32.104 ms
^C12 bytes from 64.233.162.105: icmp_seq=5, ttl=47, rtt=32.117 ms

--- 64.233.162.105 ping statistics ---
6 packets transmitted, 6 received, 0 errors, 0% packet loss, time 192.710 ms
```

```
$ sudo ./target/debug/ping 82.131.20.194
PING 82.131.20.194 4(32) bytes of data.
From 82.131.20.198 icmp_seq=0 Destination Host Unreachable
From 82.131.20.198 icmp_seq=1 Destination Host Unreachable
^C
--- 82.131.20.194 ping statistics ---
3 packets transmitted, 0 received, 2 errors, 100% packet loss, time 12036.870 ms
```

Implementation
--------------

[std::net](https://doc.rust-lang.org/std/net/) provides support for only TCP and
UDP packets, but ICMP runs on top of IP, so raw sockets are needed. Those are
provided by
[libc](https://doc.rust-lang.org/libc/x86_64-unknown-linux-gnu/libc/index.html).

Currently only IP address is supported since host-to-IP conversions are not in
stable rust.

Implementation itself is quite naïve: it expects to receive responses in order
and responses only for itself.
